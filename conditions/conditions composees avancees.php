<?php

//Exo 1
// Écrivez le test qui vérifie la question suivante : (utilisez un seul if)
echo "1.Est-ce que c'est un animal qui fait l'affaire ? \n";
echo "Je veux un chien ou chat couleur marron avec des yeux verts \n";
$animal = "Chien";
$vivant = true;
$couleur = "Marron";
$yeux = "Vert";

if( $animal == "Chat" || $animal == "Chien" && $couleur == "Marron" && $yeux == "Vert" ){
  echo 'Oui';
}else {
  echo 'Non';
}

//Exo 2
// Écrivez le test qui vérifie la question suivante : (utilisez un seul if)
echo "2.Est ce que c'est un de mes chat ? \n";
echo "Un de mes chats est vivant, a des yeux verts et est de couleur marron. Et l'autre est aussi vivant, de couleur Blanc et des yeux verts  ";

$animal = "Chat";
$vivant = true;
$couleur = "Blanc";
$yeux = "Vert";

if( $animal == "Chat" && $vivant == true && $yeux == "Vert" && ($couleur == "Marron" || $couleur == "Blanc")){
  echo 'Oui';
}else {
  echo 'Non';
}

//Exo 3
// Écrivez le test qui vérifie la question suivante : (utilisez un seul if) 
echo "3.Est ce que c'est un de mes animaux ? ";
echo "J'avais un chien de couleur noir avec des yeux bleus. J'ai un chat de couleur orange et des yeux bleus et un chien de couleur marron et des yeux bleus ";

$animal = "Chat"; 
$couleur = "Orange";
$yeux = "Bleu";

if ($animal == "Chien" && ($couleur == "Noir" || $couleur == "Marron") && $yeux == "Bleu" || ($animal == "Chat" && $couleur == "Orange" && $yeux == "Bleu")) {
  echo 'Oui';
}else {
  echo 'Non';
}

?>
