<?php

//Exo 1
echo "8.Les mois depuis le debut de l'annee : \n";

$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];
for ($i = 0; $i < count($mois); ++$i) {
  echo $mois[$i] . "\n";
}
echo "\n";

//Exo 2
//Afficher les mois de la fin de l'année jusqu'au début de l'année
$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];

echo "9.Les mois depuis la fin de l'annee : \n";
for ($i = count($mois) - 1; $i >= 0; --$i) {
  echo $mois[$i] . "\n";
}
echo "\n";


//Exo 3 
//Afficher le nom et prénoms des élèves de ce collège
$college = array(
  'Sixieme' => array(
    array('Nom' => 'Payet', 'Prenom' => 'Mickael'),
    array('Nom' => 'Hoareau', 'Prenom' => 'Christine'),
    array('Nom' => 'Maillot', 'Prenom' => 'Laure'),
  ),
  'Cinquieme' => array(
    array('Nom' => 'Bourdon', 'Prenom' => 'Didier'),
    array('Nom' => 'Legitimus', 'Prenom' => 'Pascal'),
    array('Nom' => 'Campan', 'Prenom' => 'Bernard'),
    array('Nom' => 'Fois', 'Prenom' => 'Marina'),
    array('Nom' => 'Floresti', 'Prenom' => 'Florence'),
  ),
  'Quatrieme' => array(
    array('Nom' => 'Willis', 'Prenom' => 'Bruce'),
    array('Nom' => 'Lawrence', 'Prenom' => 'Laurence'),
    array('Nom' => 'Johannson', 'Prenom' => 'Scarlett'),
    array('Nom' => 'Jackson', 'Prenom' => 'Samuel'),
  ),
);
echo '10.Les eleves du college : <br>';

//Message d'erreur dans le terminal
foreach ($college as $classe => $eleves) {
  foreach ($eleves as $a => $b) {
    echo "\n";
    foreach ($b as $c => $d) {
      echo "{$c} => {$d}" . "\n";
    }
  }
  echo "{$eleves} => {$classe} \n";
}

//Exo 4 Mal affiché - Revoir tableaux multidimensionnels

$college = array(
  'Sixieme' => array(
    array('Nom' => 'Payet', 'Prenom' => 'Mickael'),
    array('Nom' => 'Hoareau', 'Prenom' => 'Christine'),
    array('Nom' => 'Maillot', 'Prenom' => 'Laure'),
  ),
  'Cinquieme' => array(
    array('Nom' => 'Bourdon', 'Prenom' => 'Didier'),
    array('Nom' => 'Legitimus', 'Prenom' => 'Pascal'),
    array('Nom' => 'Campan', 'Prenom' => 'Bernard'),
    array('Nom' => 'Fois', 'Prenom' => 'Marina'),
    array('Nom' => 'Floresti', 'Prenom' => 'Florence'),
  ),
  'Quatrieme' => array(
    array('Nom' => 'Willis', 'Prenom' => 'Bruce'),
    array('Nom' => 'Lawrence', 'Prenom' => 'Laurence'),
    array('Nom' => 'Johannson', 'Prenom' => 'Scarlett'),
    array('Nom' => 'Jackson', 'Prenom' => 'Samuel'),
  ),
);
echo '10.Les eleves du college : <br>';

foreach ($college as $classe => $tableau) {
  foreach ($tableau as $a => $b) {
    //echo "{$classe} => {$nom} \n";
    foreach ($b as $c => $d) {
      echo " - {$classe} - {$c} => {$d} \n";
    }
  }
}

//Exo 5 - Pas réussi
//Afficher toutes les informations de la vidéothèque
$videotheque = array(
  array(
    'nom' => 'Independance day',
    'date' => 1996,
    'realisateur' => 'Roland Emmerich',
    'acteurs' => array(
      'Will Smith', 'Bill Pullman', 'Jeff Goldblum', 'Mary McDonnell',
    ),
  ),
  array(
    'nom' => 'Bienvenue a Gattaca',
    'date' => 1998,
    'realisateur' => 'Andrew Niccol',
    'acteurs' => array(
      'Ethan Hawke', 'Uma Thurman', 'Jude Law',
    ),
  ),
  array(
    'nom' => 'Forrest Gump',
    'date' => 1994,
    'realisateur' => 'Robert Zemeckis',
    'acteurs' => array(
      'Tom Hanks', 'Gary Sinise',
    ),
  ),
  array(
    'nom' => '12 hommes en colere',
    'date' => 1957,
    'realisateur' => 'Sidney Lumet',
    'acteurs' => array(
      'Henry Fonda', 'Martin Balsam', 'John Fiedler', 'Lee J. Cobb', 'E.G. Marshall',
    ),
  ),
);
echo "12.Mes films : \n";

foreach ($videotheque as $key => $val) {
  echo $key . '=>' . $val['d1'] . "=>" . $val['d2'] . '<br> ';
}
echo '<br><br>';


foreach ($tab_date as $key => $val) {
  echo $key . '=>' . $val['d1'] . "=>" . $val['d2'] . '<br> ';
}
